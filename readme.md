#  ML in browser

mini example shamelessly stolen from [a
blog](https://www.analyticsvidhya.com/blog/2019/06/build-machine-learning-model-in-your-browser-tensorflow-js-deeplearn-js/)

this demonstrates that you can do ML in your browser!

it's very simple vanilla js and html web app, using a little web server written
in go.

libraries used are ajax's p5 for the video and image handling, and ml5 for the
machine learning stuff and model, which in this case is MobileNet.

observe that this actually needs to be served locally or through httos,
otherwise modern browsers won't let you use your webcam.

interesting for me is the time it takes to load the model, which seems to be
about six seconds and might be an optimistic estimate due to possible caching
(i did not remember to check the time on first trial).
